import glob
import ctypes
import json
import os
import tkinter as tk
from tkinter import simpledialog

json_files = glob.glob("*.json")

tk.Tk().withdraw()

def grade_extend():
    for json_grade in json_files:
        with open(json_grade, 'r') as grade_module:
            grade = json.load(grade_module)['grades']
            grade_str = json.dumps(grade)[1:-1]
            grade_dictionary = json.loads(grade_str)
            
            data['grades'].append(grade_dictionary)
            data['name'] = grade_name
    
    if not os.path.exists('results'):
        os.makedirs('results')
            
    with open("results/" + str(grade_name).replace(" ", "_") + ".grade.json", "w") as outfile:    
        json.dump(data, outfile, indent=4)

if len(json_files) < 2:
    ctypes.windll.user32.MessageBoxW(0, 'No grades found in root', 'User Error', 0x30 | 0x40000)
elif 'main.json' not in json_files:
    
    data = {
        "device_businesslogic_version": 95,
        "grades": [],
        "isEdited": False,
        "library_index": 5,
        "name": ""
    }
    
    grade_name = simpledialog.askstring(title=" ", prompt="Name grade library")
    
    if grade_name == "" or grade_name == None or grade_name.isspace():
        grade_name = "No_name"
    
    grade_extend()
else:
    json_files.remove("main.json")

    with open('main.json', 'r') as main:
        data=json.load(main)
        
        grade_name = simpledialog.askstring(title=" ", prompt="(re)name grade library")
        
        if grade_name == "" or grade_name == None or grade_name.isspace():
            grade_name = data['name']+'_extended'
        
        grade_extend()